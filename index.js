import express from 'express';

const app = express();

app.get('/', (req, res) => {
  res.send('This is our third group CI/CD demo!11');
});

app.get('/api/hello', (req, res) => {
  res.json({ message: 'hello CI/CD' });
});

const port = 3000;
app.listen(port, () => {
  console.log(`The server run on http://localhost:${port}`);
});