import request from 'supertest';
import app from './index';

jest.mock('./index', () => {
  const express = require('express');
  const app = express();

  app.get('/', (req, res) => {
    res.send('This is our third group CI/CD demo');
  });

  app.get('/api/hello', (req, res) => {
    res.json({ message: 'hello CI/CD' });
  });

  return app;
});

describe('Test Root Path', () => {
  it('should return the correct message', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toBe('This is our third group CI/CD demo');
  });
});

describe('Test /api/hello Path', () => {
  it('should return the correct JSON message', async () => {
    const response = await request(app).get('/api/hello');
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: 'hello CI/CD' });
  });
});
